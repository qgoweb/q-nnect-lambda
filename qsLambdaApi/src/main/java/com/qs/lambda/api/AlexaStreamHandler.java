package com.qs.lambda.api;


import java.util.HashSet;
import java.util.Set;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;
//import com.quanto.solutions.iot.connector.tools.AlexaSpeechlet;


public class AlexaStreamHandler extends SpeechletRequestStreamHandler {

	private static final Set<String> supportedApplicationIds;

	static {
		supportedApplicationIds = new HashSet<String>();
		supportedApplicationIds.add("amzn1.ask.skill.9f39da4e-15b2-4971-baf7-c55410d04013");
	}

	public AlexaStreamHandler() {
		super(new AlexaSpeechlet(), supportedApplicationIds);
	}

	public AlexaStreamHandler(AlexaSpeechlet speechlet, Set<String> supportedApplicationIds) {
		super(speechlet, supportedApplicationIds);
	}
}