package com.qs.lambda.api;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;

public class AlexaSpeechlet implements Speechlet {

	private static final String URL_QNNECT = "http://qs-quantosolutions.rhcloud.com/QsLambda";
	private static final String SPEECH_TEMPERATURE = "Es hat %s Grad in %s.";
	private static final String SPEECH_HUMIDITY = "Die Luftfeuchtigkeit betreagt %s Prozent in %s";
	private static final String INVALID_INTENT = "Invalid Intent";
	private static final String AMAZON_CANCEL_INTENT = "AMAZON.CancelIntent";
	private static final String AMAZON_STOP_INTENT = "AMAZON.StopIntent";
	private static final String AMAZON_HELP_INTENT = "AMAZON.HelpIntent";
	private static final String LESE_TEMPERATUR = "leseTemperatur";
	private static final String BYEBYE_SPEECH = "Auf Wiedersehen";
	private static final String HELLO_SPEECH = "Hallo, wie kann ich weiterhelfen?";
	private static final String HELP_SPEECH = "Frag mich, wie viel Grad es in Stuttgart hat.";
	private static final String NO_DATA_FOR = "Leider habe ich keine Daten fuer ";
	private static final String NOT_UNDERSTANDING = "Das habe ich leider nicht verstanden. Bitte versuche es noch einmal.";
	private static final String LESE_LUFTFEUCHTIGKEIT = "leseLuftfeuchtigkeit";
	private static final String WELCOME_INTENT = "welcome";

	public SpeechletResponse onIntent(IntentRequest request, Session session) throws SpeechletException {
		Intent intent = request.getIntent();
		String intentName = (intent != null) ? intent.getName() : null;

		// choose Intent
		if (WELCOME_INTENT.equals(intentName)) {
			return createSpeechAskResponse(HELLO_SPEECH);
		} else if (LESE_LUFTFEUCHTIGKEIT.equals(intentName)) {
			String loc = request.getIntent().getSlot("Location").getValue();
			if (loc == null) {
				return createSpeechAskResponse(NOT_UNDERSTANDING);
			}
			return getHumidityResponse("humidity", loc);
		} else if (LESE_TEMPERATUR.equals(intentName)) {
			String loc = request.getIntent().getSlot("Location").getValue();
			if (loc == null) {
				return createSpeechAskResponse(NOT_UNDERSTANDING);
			}
			return getTemperatureResponse("temperature", loc);
		} else if (AMAZON_HELP_INTENT.equals(intentName)) {
			return createSpeechTellResponse(HELP_SPEECH);
		} else if (AMAZON_STOP_INTENT.equals(intentName)) {
			return createSpeechTellResponse(BYEBYE_SPEECH);
		} else if (AMAZON_CANCEL_INTENT.equals(intentName)) {
			return createSpeechTellResponse(BYEBYE_SPEECH);
		} else {
			throw new SpeechletException(INVALID_INTENT);
		}
	}

	private SpeechletResponse createSpeechTellResponse(String speechText) {
		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}

	private SpeechletResponse createSpeechAskResponse(String speechText) {
		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		// Create reprompt
		Reprompt reprompt = new Reprompt();
		reprompt.setOutputSpeech(speech);
		return SpeechletResponse.newAskResponse(speech, reprompt);
	}

	private SpeechletResponse getTemperatureResponse(String type, String loc) {
		String temp = getDataQnnect(loc, type);
		// if no Data was found reply Failure Message
		if (temp.isEmpty()) {
			return createSpeechTellResponse(NO_DATA_FOR + loc);
		}
		String speechText = String.format(SPEECH_TEMPERATURE, temp, loc);
		return createSpeechTellResponse(speechText);
	}

	private SpeechletResponse getHumidityResponse(String type, String loc) {
		String hum = getDataQnnect(loc, type);
		// if no Data was found reply Failure Message
		if (hum.isEmpty()) {
			return createSpeechTellResponse(NO_DATA_FOR + loc);
		}
		String speechText = String.format(SPEECH_HUMIDITY, hum, loc);
		return createSpeechTellResponse(speechText);
	}

	private static String getDataQnnect(String location, String type) {
		String result = "";

		try {
			String url = URL_QNNECT;

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is POST
			con.setRequestMethod("POST");
			// add request header
			con.setRequestProperty("location", location);
			con.setRequestProperty("type", type);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			result = response.toString();
		} catch (Exception e) {
			// nothing
		}
		return result;

	}

	public SpeechletResponse onLaunch(LaunchRequest request, Session session) throws SpeechletException {
		return createSpeechAskResponse(HELLO_SPEECH);
	}

	public void onSessionEnded(SessionEndedRequest request, Session session) throws SpeechletException {
		// log.info("onSessionEnded requestId={}, sessionId={}",
		// request.getRequestId(),session.getSessionId());
	}

	public void onSessionStarted(SessionStartedRequest request, Session session) throws SpeechletException {
		// log.info("onSessionEnded requestId={}, sessionId={}",
		// request.getRequestId(),session.getSessionId());
	}
}
